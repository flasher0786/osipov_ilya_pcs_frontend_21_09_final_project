import React from 'react';
import MenuList from "./components/MenuList/MenuList";
import {Basket} from "./components/Basket/Basket";
import MainPage from "./components/MainPage/MainPage";
import {Outlet, Route, Routes} from "react-router-dom";





function App() {

    return (
      <Routes>
       <Route path={'/'} element={<Layout/>}>
          <Route index element={<MainPage/>}/>
          <Route path={'menu'} element={<MenuList/>}/>
           <Route path={'menu'} element={<MenuLayout/>}>
               <Route index element={<MenuList/>}/>
               <Route path={'basket'} element={<Basket/>}/>
           </Route>
           </Route>
      </Routes>
    )
}
function MenuLayout() {
    return (
        <div className="App">
        <Outlet/>
        </div>
    );
}

function Layout() {
   return (
       <div className="App">
           <Outlet/>
       </div>
 );
}

export default App;
