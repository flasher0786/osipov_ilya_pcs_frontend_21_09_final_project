import { createSlice} from "@reduxjs/toolkit";
import {disheslist} from "../../disheslist";

const initialState = {
    menu: disheslist,
    basket: [],
    counter: 0,
    sumprice: 0
}


export  const counterSlice = createSlice({
    name: 'menu',
    initialState,
    reducers: {
        addBasket: (state, action) => {
            console.log('action', action)
            console.log('state.basket')
            state.menu.forEach(element => {
                if (element.id === action.payload) {
                    console.log('element.id and action.payload', action.payload)
                    state.basket.push(element)
                    console.log(state.basket[0].title)
                }
            });
        },
        removeCardBasket: (state, action) => {
            state.basket = state.basket.filter(function (item) {
                return item.id !== action.payload;
            });
        },
    counterProduct: (state) => {
        state.counter = state.basket.length
    },
},
});
export const { addBasket, removeCardBasket, counterProduct } = counterSlice.actions;
export default counterSlice.reducer;