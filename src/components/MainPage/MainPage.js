import React, {useEffect, useState} from "react";
import './MainPage.css';
import {useNavigate} from "react-router-dom";

const MainPage = () => {
    const [email, setEmail] = useState('')
    const [pass, setPassword] = useState('')
    const [emailDirty, setEmailDirty] = useState(false)
    const [passDirty, setPassDirty] = useState(false)
    const [emailError, setEmailError] = useState('Email не может быть пустым')
    const [passError, setPassError] = useState('Пароль не может быть пустым')
    const [formValid, setFormValid] = useState(false)
    const nextPageMenu = useNavigate();


    useEffect(() => {
        if (emailError || passError || email !== 'admin@admin.com' || pass !== '12345678') {
            setFormValid(false)
        } else {
            setFormValid(true)

        }
    }, [emailError, passError])

    const blurHandler = (e) => {
        switch (e.target.name) {
            case 'email':
                setEmailDirty(true)
                break
            case 'pass':
                setPassDirty(true)
                break
        }

    }
    const emailHandler = (e) => {
        setEmail(e.target.value)
       const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
       if (!re.test(String(e.target.value).toLowerCase() || email !== 'admin@admin.com')) {
           setEmailError('Неккоректный email')
        }
           else {
            setEmailError('')
        }
    }

    const passHandler = (e) => {
        setPassword(e.target.value)
        if (e.target.value.length < 8) {
            setPassError('Пароль не может быть меньше 8 знаков')
        } else setPassError('')
    }



        return (
            <div className={'auth-main'}>
                <form className={'auth-form'}>
                    <div className={'auth-form__frame'}>
                        <h1 className={'auth-form__title'}>Вход</h1>
                        <div className={'auth-form__email-wrapper'}>
                            <input onChange={e => emailHandler(e)} value={email} className={'auth-form__email'} type='text' name={email} placeholder="E-mail"/>
                            <div className={'emailError'}>{emailError}</div>
                        </div>
                        <div className={'auth-form__pass-wrapper'}>
                            <input onChange={e => passHandler(e)} value={pass} className={'auth-form__pass'} type='password' name={pass} placeholder="Пароль"/>
                            <div className={'passError'}>{passError}</div>
                        </div>
                        <div className={'auth-form__checkbox-wrapper'}>
                            <input className={'auth-form__checkbox'} type={'checkbox'} id={'input-checkbox'} checked/>
                            <div className="auth-form__checkbox-mark"></div>
                            <label className={'auth-form__checkbox-label'}>Я согласен получать обновления на
                                почту</label>
                        </div>
                        <button disabled={!formValid} onClick={e => nextPageMenu('menu')} className={'auth-form__button'} type="button">Войти</button>
                    </div>
                </form>
            </div>
        )
    }


export default MainPage;
