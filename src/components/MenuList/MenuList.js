import React from 'react';
import './MenuList.css';
import Card from "../FoodCard/Card";
import {useNavigate} from "react-router-dom";
import {disheslist} from "../../disheslist";
import {useSelector, useDispatch } from "react-redux";



function MenuList(props) {
    const counterProduct = useSelector((state) => state.menu.counter)
    const dispatch = useDispatch()
    const nextPageBasket = useNavigate();

  return  (
        <div className= {'menu-list'}>
        <h1 className={'menu-list_title'}>НАША ПРОДУКЦИЯ</h1>
            <div className={'menu-list_basket-info'}>
            <span >{counterProduct}</span> товара<br/>
            на сумму {}
                </div>
        <form className={'menu-list_form'}>
            <button onClick={e => nextPageBasket('basket')} className={'menu-list_button-basket'}><i className={'icon_basket'}></i></button>
        </form>

        {
            disheslist.map((food) => {
                return (
                    <Card
                        key={food.id}
                        id={food.id}
                        title={food.title}
                        info={food.info}
                        price={food.price}
                        img={food.img}/>

            )
        })
    }
</div>

)
}

export default MenuList;

