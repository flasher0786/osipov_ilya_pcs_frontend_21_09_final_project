import React from 'react';
import './Card.css';
import { useSelector, useDispatch } from "react-redux";
import { addBasket } from "../../app/reducers/counter";
import {counterProduct} from "../../app/reducers/counter";

const Card = (props) => {
const dispatch = useDispatch()
    function dispatchReducers() {
    dispatch(addBasket(props.id))
        dispatch(counterProduct())
    }

return (
    <div className={'card'}>
        <div className={'card_image'}>
            <img src={props.img} alt={''}/>
        </div>
        <div className={'card_title'}>
            <h3>{props.title}</h3></div>
        <div className={'card_info'}>{props.info}</div>
        <div className={'card_price'}>{props.price}
            <form>
                <button type={"button"} onClick={(event) => dispatchReducers()} className={'card_button'}>+</button>
            </form>
        </div>
    </div>
)
}



export default Card;


/*export default class Card extends React.Component {
    static defaultProps = {
       id: 0,
        titleFood: '',
        infoFood: '',
        priceFood: '',
        imgFood: ''
    }
    render() {
        const { id, titleFood, infoFood, priceFood, imgFood } = this.props;
        return (
            <div className={'card'}>
                <div className={'card_image'}>
                <img src={imgFood}/>
                </div>
                <div className={'card_title'}>
                    <h3>{titleFood}</h3></div>
                <div className={'card_info'}>{infoFood}</div>
                <div className={'card_price'}>{priceFood}
                <form>
                    <button onClick={e => (id)} className={'card_button'}>+</button>
                </form>
                </div>
            </div>
        );
    }
 }
*/