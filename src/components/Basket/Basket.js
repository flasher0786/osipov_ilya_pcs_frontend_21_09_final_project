import React from "react";
import './Basket.css';
import { useSelector } from "react-redux";
import { BasketCard } from "../BasketCard/basketCard";


const Basket = (props) => {
    const basketMenu = useSelector((state) => state.menu.basket)

    return (

        <div className={'basket-form'}>
            <h1 className={'basket-form_title'}>КОРЗИНА С ВЫБРАННЫМИ ТОВАРАМИ</h1>

            <div className={'basket-form_foodlist'}>

                {
                    basketMenu.map((item) => {
                            return (
                                <BasketCard
                                    id={item.id}
                                    title={item.title}
                                    price={item.price}
                                    img={item.img}/>
                            )
                        }
                    )}


            </div>
        </div>

    )
}


export {Basket};