import React from 'react';
import './basketCard.css';
import { removeCardBasket } from "../../app/reducers/counter";
import { useDispatch } from "react-redux";

const BasketCard = (props) => {
    const dispatch = useDispatch()
    return (
        <div className={'basket-card'}>
            <div className={''}>
                <img src={props.img} alt={''}/></div>
                  <div className={''}>
                     <h3>{props.title}</h3></div>
            <div className={''}>{props.price}</div>
            <button onClick={(e) => dispatch(removeCardBasket(props.id))} className={'basket-card_button'}>-</button>
        </div>
    )
}
export { BasketCard };